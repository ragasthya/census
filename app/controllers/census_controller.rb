class CensusController < ApplicationController
  before_action :set_censu, only: [:show, :edit, :update, :destroy]

  # GET /census
  # GET /census.json
  def index
    @census = Censu.all
  end

  # GET /census/1
  # GET /census/1.json
  def show
  end

  # GET /census/new
  def new
    @censu = Censu.new
  end

  # GET /census/1/edit
  def edit
  end

  # POST /census
  # POST /census.json
  def create
    @censu = Censu.new(censu_params)

    respond_to do |format|
      if @censu.save
        format.html { redirect_to @censu, notice: 'Censu was successfully created.' }
        format.json { render :show, status: :created, location: @censu }
      else
        format.html { render :new }
        format.json { render json: @censu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /census/1
  # PATCH/PUT /census/1.json
  def update
    respond_to do |format|
      if @censu.update(censu_params)
        format.html { redirect_to @censu, notice: 'Censu was successfully updated.' }
        format.json { render :show, status: :ok, location: @censu }
      else
        format.html { render :edit }
        format.json { render json: @censu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /census/1
  # DELETE /census/1.json
  def destroy
    @censu.destroy
    respond_to do |format|
      format.html { redirect_to census_url, notice: 'Censu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_censu
      @censu = Censu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def censu_params
      params.require(:censu).permit(:date_start, :date,, :date_end, :date,, :year, :string)
    end
end
