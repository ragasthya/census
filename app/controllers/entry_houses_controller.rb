class EntryHousesController < ApplicationController
  before_action :set_entry_house, only: [:show, :edit, :update, :destroy]

  # GET /entry_houses
  # GET /entry_houses.json
  def index
    @entry_houses = EntryHouse.all
  end

  # GET /entry_houses/1
  # GET /entry_houses/1.json
  def show
  end

  # GET /entry_houses/new
  def new
    @entry_house = EntryHouse.new
  end

  # GET /entry_houses/1/edit
  def edit
  end

  # POST /entry_houses
  # POST /entry_houses.json
  def create
    @entry_house = EntryHouse.new(entry_house_params)

    respond_to do |format|
      if @entry_house.save
        format.html { redirect_to @entry_house, notice: 'Entry house was successfully created.' }
        format.json { render :show, status: :created, location: @entry_house }
      else
        format.html { render :new }
        format.json { render json: @entry_house.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entry_houses/1
  # PATCH/PUT /entry_houses/1.json
  def update
    respond_to do |format|
      if @entry_house.update(entry_house_params)
        format.html { redirect_to @entry_house, notice: 'Entry house was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry_house }
      else
        format.html { render :edit }
        format.json { render json: @entry_house.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entry_houses/1
  # DELETE /entry_houses/1.json
  def destroy
    @entry_house.destroy
    respond_to do |format|
      format.html { redirect_to entry_houses_url, notice: 'Entry house was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry_house
      @entry_house = EntryHouse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_house_params
      params.require(:entry_house).permit(:address, :housing_schedule, :census_house_number, :predominant_material_floor, :predominant_material_wall, :predominant_material_roof, :house_use, :actual_house_use, :condition, :house_number, :residing_m, :residing_f, :residing_o, :house_head, :gender, :backward, :ownership, :dwelling_rooms, :married_couples, :drinking_water, :lighting, :toilet, :sewage_connection, :bathing, :kitchen, :cooking_fuel, :radio, :television, :computer, :telephone, :bicycle, :moped, :car, :banking)
    end
end
