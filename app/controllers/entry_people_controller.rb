class EntryPeopleController < ApplicationController
  before_action :set_entry_person, only: [:show, :edit, :update, :destroy]

  # GET /entry_people
  # GET /entry_people.json
  def index
    @entry_people = EntryPerson.all
  end

  # GET /entry_people/1
  # GET /entry_people/1.json
  def show
  end

  # GET /entry_people/new
  def new
    @entry_person = EntryPerson.new
  end

  # GET /entry_people/1/edit
  def edit
  end

  # POST /entry_people
  # POST /entry_people.json
  def create
    @entry_person = EntryPerson.new(entry_person_params)

    respond_to do |format|
      if @entry_person.save
        format.html { redirect_to @entry_person, notice: 'Entry person was successfully created.' }
        format.json { render :show, status: :created, location: @entry_person }
      else
        format.html { render :new }
        format.json { render json: @entry_person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entry_people/1
  # PATCH/PUT /entry_people/1.json
  def update
    respond_to do |format|
      if @entry_person.update(entry_person_params)
        format.html { redirect_to @entry_person, notice: 'Entry person was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry_person }
      else
        format.html { render :edit }
        format.json { render json: @entry_person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entry_people/1
  # DELETE /entry_people/1.json
  def destroy
    @entry_person.destroy
    respond_to do |format|
      format.html { redirect_to entry_people_url, notice: 'Entry person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry_person
      @entry_person = EntryPerson.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_person_params
      params.require(:entry_person).permit(:household_schedule, :name, :is_head, :gender, :date_of_birth, :age, :marital_status, :age_at_marriage, :religion, :backward, :disability, :is_literate, :attendence, :highest_education, :employment_status, :employment_type, :occupation, :company_type, :employee_class, :non_economic_activity, :employment_seeking, :mode_of_travel, :birth_place, :last_residence, :residing_since, :migration_reason, :surviving_d, :surviving_s, :born_d, :born_s, :recent_birth_d, :recent_birth_s)
    end
end
