class HouseholdSchedulesController < ApplicationController
  before_action :set_household_schedule, only: [:show, :edit, :update, :destroy]

  # GET /household_schedules
  # GET /household_schedules.json
  def index
    @household_schedules = HouseholdSchedule.all
  end

  # GET /household_schedules/1
  # GET /household_schedules/1.json
  def show
  end

  # GET /household_schedules/new
  def new
    @household_schedule = HouseholdSchedule.new
  end

  # GET /household_schedules/1/edit
  def edit
  end

  # POST /household_schedules
  # POST /household_schedules.json
  def create
    @household_schedule = HouseholdSchedule.new(household_schedule_params)

    respond_to do |format|
      if @household_schedule.save
        format.html { redirect_to @household_schedule, notice: 'Household schedule was successfully created.' }
        format.json { render :show, status: :created, location: @household_schedule }
      else
        format.html { render :new }
        format.json { render json: @household_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /household_schedules/1
  # PATCH/PUT /household_schedules/1.json
  def update
    respond_to do |format|
      if @household_schedule.update(household_schedule_params)
        format.html { redirect_to @household_schedule, notice: 'Household schedule was successfully updated.' }
        format.json { render :show, status: :ok, location: @household_schedule }
      else
        format.html { render :edit }
        format.json { render json: @household_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /household_schedules/1
  # DELETE /household_schedules/1.json
  def destroy
    @household_schedule.destroy
    respond_to do |format|
      format.html { redirect_to household_schedules_url, notice: 'Household schedule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_household_schedule
      @household_schedule = HouseholdSchedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def household_schedule_params
      params.require(:household_schedule).permit(:address, :form_number, :status, :enumeration_block_number, :enumeration_sub_block_number, :houselist_block_number, :serial_number, :type, :details, :population_m, :population_f, :population_o, :young_m, :young_f, :young_o, :literate_m, :literate_f, :literate_o, :illiterate_m, :illiterate_f, :illiterate_o, :respondent, :email, :phone, :enumerator, :supervisor, :filing_date, :census)
    end
end
