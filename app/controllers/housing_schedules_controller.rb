class HousingSchedulesController < ApplicationController
  before_action :set_housing_schedule, only: [:show, :edit, :update, :destroy]

  # GET /housing_schedules
  # GET /housing_schedules.json
  def index
    @housing_schedules = HousingSchedule.all
  end

  # GET /housing_schedules/1
  # GET /housing_schedules/1.json
  def show
  end

  # GET /housing_schedules/new
  def new
    @housing_schedule = HousingSchedule.new
  end

  # GET /housing_schedules/1/edit
  def edit
  end

  # POST /housing_schedules
  # POST /housing_schedules.json
  def create
    @housing_schedule = HousingSchedule.new(housing_schedule_params)

    respond_to do |format|
      if @housing_schedule.save
        format.html { redirect_to @housing_schedule, notice: 'Housing schedule was successfully created.' }
        format.json { render :show, status: :created, location: @housing_schedule }
      else
        format.html { render :new }
        format.json { render json: @housing_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /housing_schedules/1
  # PATCH/PUT /housing_schedules/1.json
  def update
    respond_to do |format|
      if @housing_schedule.update(housing_schedule_params)
        format.html { redirect_to @housing_schedule, notice: 'Housing schedule was successfully updated.' }
        format.json { render :show, status: :ok, location: @housing_schedule }
      else
        format.html { render :edit }
        format.json { render json: @housing_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /housing_schedules/1
  # DELETE /housing_schedules/1.json
  def destroy
    @housing_schedule.destroy
    respond_to do |format|
      format.html { redirect_to housing_schedules_url, notice: 'Housing schedule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_housing_schedule
      @housing_schedule = HousingSchedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def housing_schedule_params
      params.require(:housing_schedule).permit(:address, :form_number, :status, :enumeration_block_number, :enumeration_sub_block_number, :houselist_block_number, :serial_number, :respondent, :email, :phone, :enumerator, :supervisor, :filing_date, :census)
    end
end
