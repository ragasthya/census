json.extract! address, :id, :house_number, :first_line, :locality, :city, :sub_district, :ward_number, :postal_code, :created_at, :updated_at
json.url address_url(address, format: :json)
