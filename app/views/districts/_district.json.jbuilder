json.extract! district, :id, :name, :population, :area, :density, :state, :created_at, :updated_at
json.url district_url(district, format: :json)
