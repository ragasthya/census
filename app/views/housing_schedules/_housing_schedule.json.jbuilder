json.extract! housing_schedule, :id, :address, :form_number, :status, :enumeration_block_number, :enumeration_sub_block_number, :houselist_block_number, :serial_number, :respondent, :email, :phone, :enumerator, :supervisor, :filing_date, :census, :created_at, :updated_at
json.url housing_schedule_url(housing_schedule, format: :json)
