json.extract! language, :id, :entry_person, :language, :canRead, :canWrite, :canSpeak, :isMotherTongue, :created_at, :updated_at
json.url language_url(language, format: :json)
