json.extract! officer, :id, :name, :badge_number, :designation, :email, :official_contact, :access, :created_at, :updated_at
json.url officer_url(officer, format: :json)
