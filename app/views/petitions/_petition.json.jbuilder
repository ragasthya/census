json.extract! petition, :id, :form_number, :form_type, :form, :details, :timestamp, :status, :message, :officer, :created_at, :updated_at
json.url petition_url(petition, format: :json)
