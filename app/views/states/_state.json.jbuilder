json.extract! state, :id, :code, :name, :population, :area, :density, :created_at, :updated_at
json.url state_url(state, format: :json)
