json.extract! sub_district, :id, :name, :population, :area, :density, :district, :created_at, :updated_at
json.url sub_district_url(sub_district, format: :json)
