Rails.application.routes.draw do
  resources :petitions
  resources :logs
  resources :entry_houses
  resources :housing_schedules
  resources :languages
  resources :entry_people
  resources :household_schedules
  resources :addresses
  resources :officers
  resources :sub_districts
  resources :districts
  resources :states
  resources :census
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
