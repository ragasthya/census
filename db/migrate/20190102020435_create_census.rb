class CreateCensus < ActiveRecord::Migration[5.2]
  def change
    create_table :census do |t|
      t.string :date_start
      t.string :date
      t.string :date_end
      t.string :date
      t.string :year
      t.string :string

      t.timestamps
    end
  end
end
