class CreateStates < ActiveRecord::Migration[5.2]
  def change
    create_table :states do |t|
      t.string :code
      t.string :name
      t.integer :population
      t.float :area
      t.float :density
      t.integer :officer
      t.timestamps
    end
  end
end
