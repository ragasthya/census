class CreateDistricts < ActiveRecord::Migration[5.2]
  def change
    create_table :districts do |t|
      t.string :name
      t.integer :population
      t.float :area
      t.float :density
      t.integer :state
      t.integer :officer

      t.timestamps
    end
  end
end
