class CreateOfficers < ActiveRecord::Migration[5.2]
  def change
    create_table :officers do |t|
      t.string :name
      t.string :badge_number
      t.string :designation
      t.string :email
      t.string :official_contact
      t.string :access

      t.timestamps
    end
  end
end
