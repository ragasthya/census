class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :house_number
      t.string :first_line
      t.string :locality
      t.string :city
      t.integer :sub_district
      t.string :ward_number
      t.string :postal_code

      t.timestamps
    end
  end
end
