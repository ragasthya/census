class CreateHouseholdSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :household_schedules do |t|
      t.integer :address
      t.string :form_number
      t.string :status
      t.string :enumeration_block_number
      t.string :enumeration_sub_block_number
      t.string :houselist_block_number
      t.string :serial_number
      t.string :type
      t.text :details
      t.integer :population_m
      t.integer :population_f
      t.integer :population_o
      t.integer :young_m
      t.integer :young_f
      t.integer :young_o
      t.integer :literate_m
      t.integer :literate_f
      t.integer :literate_o
      t.integer :illiterate_m
      t.integer :illiterate_f
      t.integer :illiterate_o
      t.string :respondent
      t.string :email
      t.string :phone
      t.integer :enumerator
      t.integer :supervisor
      t.datetime :filing_date
      t.integer :census

      t.timestamps
    end
  end
end
