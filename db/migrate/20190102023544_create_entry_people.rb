class CreateEntryPeople < ActiveRecord::Migration[5.2]
  def change
    create_table :entry_people do |t|
      t.integer :household_schedule
      t.string :name
      t.boolean :is_head
      t.string :gender
      t.date :date_of_birth
      t.string :age
      t.string :marital_status
      t.string :age_at_marriage
      t.string :religion
      t.string :backward
      t.string :disability
      t.boolean :is_literate
      t.string :attendence
      t.string :highest_education
      t.string :employment_status
      t.string :employment_type
      t.string :occupation
      t.string :company_type
      t.string :employee_class
      t.string :non_economic_activity
      t.boolean :employment_seeking
      t.string :mode_of_travel
      t.integer :birth_place
      t.integer :last_residence
      t.string :residing_since
      t.text :migration_reason
      t.integer :surviving_d
      t.integer :surviving_s
      t.integer :born_d
      t.integer :born_s
      t.integer :recent_birth_d
      t.integer :recent_birth_s

      t.timestamps
    end
  end
end
