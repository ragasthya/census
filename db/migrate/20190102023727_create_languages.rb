class CreateLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :languages do |t|
      t.integer :entry_person
      t.string :language
      t.boolean :canRead
      t.boolean :canWrite
      t.boolean :canSpeak
      t.boolean :isMotherTongue

      t.timestamps
    end
  end
end
