class CreateHousingSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :housing_schedules do |t|
      t.integer :address
      t.string :form_number
      t.string :status
      t.string :enumeration_block_number
      t.string :enumeration_sub_block_number
      t.string :houselist_block_number
      t.string :serial_number
      t.string :respondent
      t.string :email
      t.string :phone
      t.integer :enumerator
      t.integer :supervisor
      t.datetime :filing_date
      t.integer :census

      t.timestamps
    end
  end
end
