class CreateEntryHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :entry_houses do |t|
      t.integer :address
      t.integer :housing_schedule
      t.string :census_house_number
      t.string :predominant_material_floor
      t.string :predominant_material_wall
      t.string :predominant_material_roof
      t.string :house_use
      t.string :actual_house_use
      t.string :condition
      t.string :house_number
      t.integer :residing_m
      t.integer :residing_f
      t.integer :residing_o
      t.string :house_head
      t.string :gender
      t.string :backward
      t.string :ownership
      t.integer :dwelling_rooms
      t.integer :married_couples
      t.string :drinking_water
      t.string :lighting
      t.string :toilet
      t.string :sewage_connection
      t.string :bathing
      t.string :kitchen
      t.string :cooking_fuel
      t.string :radio
      t.string :television
      t.string :computer
      t.string :telephone
      t.string :bicycle
      t.string :moped
      t.string :car
      t.string :banking

      t.timestamps
    end
  end
end
