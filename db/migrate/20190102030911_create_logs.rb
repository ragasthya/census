class CreateLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :logs do |t|
      t.integer :officer
      t.text :details
      t.datetime :timestamp

      t.timestamps
    end
  end
end
