class CreatePetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :petitions do |t|
      t.string :form_number
      t.string :form_type
      t.integer :form
      t.text :details
      t.datetime :timestamp
      t.string :status
      t.text :message
      t.integer :officer

      t.timestamps
    end
  end
end
