# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_02_031043) do

  create_table "addresses", force: :cascade do |t|
    t.string "house_number"
    t.string "first_line"
    t.string "locality"
    t.string "city"
    t.integer "sub_district"
    t.string "ward_number"
    t.string "postal_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "census", force: :cascade do |t|
    t.string "date_start"
    t.string "date"
    t.string "date_end"
    t.string "year"
    t.string "string"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "districts", force: :cascade do |t|
    t.string "name"
    t.integer "population"
    t.float "area"
    t.float "density"
    t.integer "state"
    t.integer "officer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entry_houses", force: :cascade do |t|
    t.integer "address"
    t.integer "housing_schedule"
    t.string "census_house_number"
    t.string "predominant_material_floor"
    t.string "predominant_material_wall"
    t.string "predominant_material_roof"
    t.string "house_use"
    t.string "actual_house_use"
    t.string "condition"
    t.string "house_number"
    t.integer "residing_m"
    t.integer "residing_f"
    t.integer "residing_o"
    t.string "house_head"
    t.string "gender"
    t.string "backward"
    t.string "ownership"
    t.integer "dwelling_rooms"
    t.integer "married_couples"
    t.string "drinking_water"
    t.string "lighting"
    t.string "toilet"
    t.string "sewage_connection"
    t.string "bathing"
    t.string "kitchen"
    t.string "cooking_fuel"
    t.string "radio"
    t.string "television"
    t.string "computer"
    t.string "telephone"
    t.string "bicycle"
    t.string "moped"
    t.string "car"
    t.string "banking"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entry_people", force: :cascade do |t|
    t.integer "household_schedule"
    t.string "name"
    t.boolean "is_head"
    t.string "gender"
    t.date "date_of_birth"
    t.string "age"
    t.string "marital_status"
    t.string "age_at_marriage"
    t.string "religion"
    t.string "backward"
    t.string "disability"
    t.boolean "is_literate"
    t.string "attendence"
    t.string "highest_education"
    t.string "employment_status"
    t.string "employment_type"
    t.string "occupation"
    t.string "company_type"
    t.string "employee_class"
    t.string "non_economic_activity"
    t.boolean "employment_seeking"
    t.string "mode_of_travel"
    t.integer "birth_place"
    t.integer "last_residence"
    t.string "residing_since"
    t.text "migration_reason"
    t.integer "surviving_d"
    t.integer "surviving_s"
    t.integer "born_d"
    t.integer "born_s"
    t.integer "recent_birth_d"
    t.integer "recent_birth_s"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "household_schedules", force: :cascade do |t|
    t.integer "address"
    t.string "form_number"
    t.string "status"
    t.string "enumeration_block_number"
    t.string "enumeration_sub_block_number"
    t.string "houselist_block_number"
    t.string "serial_number"
    t.string "type"
    t.text "details"
    t.integer "population_m"
    t.integer "population_f"
    t.integer "population_o"
    t.integer "young_m"
    t.integer "young_f"
    t.integer "young_o"
    t.integer "literate_m"
    t.integer "literate_f"
    t.integer "literate_o"
    t.integer "illiterate_m"
    t.integer "illiterate_f"
    t.integer "illiterate_o"
    t.string "respondent"
    t.string "email"
    t.string "phone"
    t.integer "enumerator"
    t.integer "supervisor"
    t.datetime "filing_date"
    t.integer "census"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "housing_schedules", force: :cascade do |t|
    t.integer "address"
    t.string "form_number"
    t.string "status"
    t.string "enumeration_block_number"
    t.string "enumeration_sub_block_number"
    t.string "houselist_block_number"
    t.string "serial_number"
    t.string "respondent"
    t.string "email"
    t.string "phone"
    t.integer "enumerator"
    t.integer "supervisor"
    t.datetime "filing_date"
    t.integer "census"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "languages", force: :cascade do |t|
    t.integer "entry_person"
    t.string "language"
    t.boolean "canRead"
    t.boolean "canWrite"
    t.boolean "canSpeak"
    t.boolean "isMotherTongue"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logs", force: :cascade do |t|
    t.integer "officer"
    t.text "details"
    t.datetime "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "officers", force: :cascade do |t|
    t.string "name"
    t.string "badge_number"
    t.string "designation"
    t.string "email"
    t.string "official_contact"
    t.string "access"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "petitions", force: :cascade do |t|
    t.string "form_number"
    t.string "form_type"
    t.integer "form"
    t.text "details"
    t.datetime "timestamp"
    t.string "status"
    t.text "message"
    t.integer "officer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.integer "population"
    t.float "area"
    t.float "density"
    t.integer "officer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sub_districts", force: :cascade do |t|
    t.string "name"
    t.integer "population"
    t.float "area"
    t.float "density"
    t.integer "district"
    t.integer "officer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
