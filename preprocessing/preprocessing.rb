require 'json'

states = []
districts = []
sub_districts = []

class State
  attr_accessor :name, :coordinares, :id

  def initialize(id, name, coordinates)
    @id = id
    @name = name
    @coordinares = coordinates
  end
end

class District
  attr_accessor :state, :name, :coordinares, :id

  def initialize(id, name, state, coordinates)
    @id = id
    @name = name
    @state = state
    @coordinares = coordinates

    puts @state.name + " " + @name
  end
end

class SubDistrict
  attr_accessor :name, :coordinares, :district, :id

  def initialize(id, name, district, coordinates)
    @id = id
    @name = name
    @district = district
    @coordinares = coordinates
  end
end

def makeAsArray(coordinates)
  coordinates_array = []
  array = (0..coordinates.length - 1).to_a
  puts coordinates.length
  item = 0
  while(true)
    if(item >= coordinates.length)
      break
    end
    coordinates_array << ([coordinates[item], coordinates[item+1]])
    item += 2
  end
  return coordinates_array
end

def findState(state_id, states)
  state_to_ret = nil
  for state in states
    if(state.id == state_id)
      state_to_ret = state
    end
  end
  return state_to_ret
end

file = File.read('states_india.geojson')
data_hash = JSON.parse(file)

states_json = data_hash['features']
for state_json in states_json
  properties = state_json['properties']
  id = properties['ID_1']
  state_name = properties['NAME_1']
  geometry = state_json['geometry']
  coordinates = geometry['coordinates']
  state = State.new(id, state_name, makeAsArray(coordinates))
  states << state
end

file = File.read('districts_india.geojson')
data_hash = JSON.parse(file)

districts_json = data_hash['features']
for district_json in districts_json
  properties = district_json['properties']
  state_id = properties['ID_1']
  district_id = properties['ID_2']
  district_name = properties['NAME_2']
  geometry = district_json['geometry']
  coordinates = geometry['coordinates']
  district = District.new(district_id, district_name, findState(state_id, states), makeAsArray(coordinates))
  districts << district
end