require 'test_helper'

class EntryHousesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @entry_house = entry_houses(:one)
  end

  test "should get index" do
    get entry_houses_url
    assert_response :success
  end

  test "should get new" do
    get new_entry_house_url
    assert_response :success
  end

  test "should create entry_house" do
    assert_difference('EntryHouse.count') do
      post entry_houses_url, params: { entry_house: { actual_house_use: @entry_house.actual_house_use, address: @entry_house.address, backward: @entry_house.backward, banking: @entry_house.banking, bathing: @entry_house.bathing, bicycle: @entry_house.bicycle, car: @entry_house.car, census_house_number: @entry_house.census_house_number, computer: @entry_house.computer, condition: @entry_house.condition, cooking_fuel: @entry_house.cooking_fuel, drinking_water: @entry_house.drinking_water, dwelling_rooms: @entry_house.dwelling_rooms, gender: @entry_house.gender, house_head: @entry_house.house_head, house_number: @entry_house.house_number, house_use: @entry_house.house_use, housing_schedule: @entry_house.housing_schedule, kitchen: @entry_house.kitchen, lighting: @entry_house.lighting, married_couples: @entry_house.married_couples, moped: @entry_house.moped, ownership: @entry_house.ownership, predominant_material_floor: @entry_house.predominant_material_floor, predominant_material_roof: @entry_house.predominant_material_roof, predominant_material_wall: @entry_house.predominant_material_wall, radio: @entry_house.radio, residing_f: @entry_house.residing_f, residing_m: @entry_house.residing_m, residing_o: @entry_house.residing_o, sewage_connection: @entry_house.sewage_connection, telephone: @entry_house.telephone, television: @entry_house.television, toilet: @entry_house.toilet } }
    end

    assert_redirected_to entry_house_url(EntryHouse.last)
  end

  test "should show entry_house" do
    get entry_house_url(@entry_house)
    assert_response :success
  end

  test "should get edit" do
    get edit_entry_house_url(@entry_house)
    assert_response :success
  end

  test "should update entry_house" do
    patch entry_house_url(@entry_house), params: { entry_house: { actual_house_use: @entry_house.actual_house_use, address: @entry_house.address, backward: @entry_house.backward, banking: @entry_house.banking, bathing: @entry_house.bathing, bicycle: @entry_house.bicycle, car: @entry_house.car, census_house_number: @entry_house.census_house_number, computer: @entry_house.computer, condition: @entry_house.condition, cooking_fuel: @entry_house.cooking_fuel, drinking_water: @entry_house.drinking_water, dwelling_rooms: @entry_house.dwelling_rooms, gender: @entry_house.gender, house_head: @entry_house.house_head, house_number: @entry_house.house_number, house_use: @entry_house.house_use, housing_schedule: @entry_house.housing_schedule, kitchen: @entry_house.kitchen, lighting: @entry_house.lighting, married_couples: @entry_house.married_couples, moped: @entry_house.moped, ownership: @entry_house.ownership, predominant_material_floor: @entry_house.predominant_material_floor, predominant_material_roof: @entry_house.predominant_material_roof, predominant_material_wall: @entry_house.predominant_material_wall, radio: @entry_house.radio, residing_f: @entry_house.residing_f, residing_m: @entry_house.residing_m, residing_o: @entry_house.residing_o, sewage_connection: @entry_house.sewage_connection, telephone: @entry_house.telephone, television: @entry_house.television, toilet: @entry_house.toilet } }
    assert_redirected_to entry_house_url(@entry_house)
  end

  test "should destroy entry_house" do
    assert_difference('EntryHouse.count', -1) do
      delete entry_house_url(@entry_house)
    end

    assert_redirected_to entry_houses_url
  end
end
