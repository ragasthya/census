require 'test_helper'

class EntryPeopleControllerTest < ActionDispatch::IntegrationTest
  setup do
    @entry_person = entry_people(:one)
  end

  test "should get index" do
    get entry_people_url
    assert_response :success
  end

  test "should get new" do
    get new_entry_person_url
    assert_response :success
  end

  test "should create entry_person" do
    assert_difference('EntryPerson.count') do
      post entry_people_url, params: { entry_person: { age: @entry_person.age, age_at_marriage: @entry_person.age_at_marriage, attendence: @entry_person.attendence, backward: @entry_person.backward, birth_place: @entry_person.birth_place, born_d: @entry_person.born_d, born_s: @entry_person.born_s, company_type: @entry_person.company_type, date_of_birth: @entry_person.date_of_birth, disability: @entry_person.disability, employee_class: @entry_person.employee_class, employment_seeking: @entry_person.employment_seeking, employment_status: @entry_person.employment_status, employment_type: @entry_person.employment_type, gender: @entry_person.gender, highest_education: @entry_person.highest_education, household_schedule: @entry_person.household_schedule, is_head: @entry_person.is_head, is_literate: @entry_person.is_literate, last_residence: @entry_person.last_residence, marital_status: @entry_person.marital_status, migration_reason: @entry_person.migration_reason, mode_of_travel: @entry_person.mode_of_travel, name: @entry_person.name, non_economic_activity: @entry_person.non_economic_activity, occupation: @entry_person.occupation, recent_birth_d: @entry_person.recent_birth_d, recent_birth_s: @entry_person.recent_birth_s, religion: @entry_person.religion, residing_since: @entry_person.residing_since, surviving_d: @entry_person.surviving_d, surviving_s: @entry_person.surviving_s } }
    end

    assert_redirected_to entry_person_url(EntryPerson.last)
  end

  test "should show entry_person" do
    get entry_person_url(@entry_person)
    assert_response :success
  end

  test "should get edit" do
    get edit_entry_person_url(@entry_person)
    assert_response :success
  end

  test "should update entry_person" do
    patch entry_person_url(@entry_person), params: { entry_person: { age: @entry_person.age, age_at_marriage: @entry_person.age_at_marriage, attendence: @entry_person.attendence, backward: @entry_person.backward, birth_place: @entry_person.birth_place, born_d: @entry_person.born_d, born_s: @entry_person.born_s, company_type: @entry_person.company_type, date_of_birth: @entry_person.date_of_birth, disability: @entry_person.disability, employee_class: @entry_person.employee_class, employment_seeking: @entry_person.employment_seeking, employment_status: @entry_person.employment_status, employment_type: @entry_person.employment_type, gender: @entry_person.gender, highest_education: @entry_person.highest_education, household_schedule: @entry_person.household_schedule, is_head: @entry_person.is_head, is_literate: @entry_person.is_literate, last_residence: @entry_person.last_residence, marital_status: @entry_person.marital_status, migration_reason: @entry_person.migration_reason, mode_of_travel: @entry_person.mode_of_travel, name: @entry_person.name, non_economic_activity: @entry_person.non_economic_activity, occupation: @entry_person.occupation, recent_birth_d: @entry_person.recent_birth_d, recent_birth_s: @entry_person.recent_birth_s, religion: @entry_person.religion, residing_since: @entry_person.residing_since, surviving_d: @entry_person.surviving_d, surviving_s: @entry_person.surviving_s } }
    assert_redirected_to entry_person_url(@entry_person)
  end

  test "should destroy entry_person" do
    assert_difference('EntryPerson.count', -1) do
      delete entry_person_url(@entry_person)
    end

    assert_redirected_to entry_people_url
  end
end
