require 'test_helper'

class HouseholdSchedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @household_schedule = household_schedules(:one)
  end

  test "should get index" do
    get household_schedules_url
    assert_response :success
  end

  test "should get new" do
    get new_household_schedule_url
    assert_response :success
  end

  test "should create household_schedule" do
    assert_difference('HouseholdSchedule.count') do
      post household_schedules_url, params: { household_schedule: { address: @household_schedule.address, census: @household_schedule.census, details: @household_schedule.details, email: @household_schedule.email, enumeration_block_number: @household_schedule.enumeration_block_number, enumeration_sub_block_number: @household_schedule.enumeration_sub_block_number, enumerator: @household_schedule.enumerator, filing_date: @household_schedule.filing_date, form_number: @household_schedule.form_number, houselist_block_number: @household_schedule.houselist_block_number, illiterate_f: @household_schedule.illiterate_f, illiterate_m: @household_schedule.illiterate_m, illiterate_o: @household_schedule.illiterate_o, literate_f: @household_schedule.literate_f, literate_m: @household_schedule.literate_m, literate_o: @household_schedule.literate_o, phone: @household_schedule.phone, population_f: @household_schedule.population_f, population_m: @household_schedule.population_m, population_o: @household_schedule.population_o, respondent: @household_schedule.respondent, serial_number: @household_schedule.serial_number, status: @household_schedule.status, supervisor: @household_schedule.supervisor, type: @household_schedule.type, young_f: @household_schedule.young_f, young_m: @household_schedule.young_m, young_o: @household_schedule.young_o } }
    end

    assert_redirected_to household_schedule_url(HouseholdSchedule.last)
  end

  test "should show household_schedule" do
    get household_schedule_url(@household_schedule)
    assert_response :success
  end

  test "should get edit" do
    get edit_household_schedule_url(@household_schedule)
    assert_response :success
  end

  test "should update household_schedule" do
    patch household_schedule_url(@household_schedule), params: { household_schedule: { address: @household_schedule.address, census: @household_schedule.census, details: @household_schedule.details, email: @household_schedule.email, enumeration_block_number: @household_schedule.enumeration_block_number, enumeration_sub_block_number: @household_schedule.enumeration_sub_block_number, enumerator: @household_schedule.enumerator, filing_date: @household_schedule.filing_date, form_number: @household_schedule.form_number, houselist_block_number: @household_schedule.houselist_block_number, illiterate_f: @household_schedule.illiterate_f, illiterate_m: @household_schedule.illiterate_m, illiterate_o: @household_schedule.illiterate_o, literate_f: @household_schedule.literate_f, literate_m: @household_schedule.literate_m, literate_o: @household_schedule.literate_o, phone: @household_schedule.phone, population_f: @household_schedule.population_f, population_m: @household_schedule.population_m, population_o: @household_schedule.population_o, respondent: @household_schedule.respondent, serial_number: @household_schedule.serial_number, status: @household_schedule.status, supervisor: @household_schedule.supervisor, type: @household_schedule.type, young_f: @household_schedule.young_f, young_m: @household_schedule.young_m, young_o: @household_schedule.young_o } }
    assert_redirected_to household_schedule_url(@household_schedule)
  end

  test "should destroy household_schedule" do
    assert_difference('HouseholdSchedule.count', -1) do
      delete household_schedule_url(@household_schedule)
    end

    assert_redirected_to household_schedules_url
  end
end
