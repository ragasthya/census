require 'test_helper'

class HousingSchedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @housing_schedule = housing_schedules(:one)
  end

  test "should get index" do
    get housing_schedules_url
    assert_response :success
  end

  test "should get new" do
    get new_housing_schedule_url
    assert_response :success
  end

  test "should create housing_schedule" do
    assert_difference('HousingSchedule.count') do
      post housing_schedules_url, params: { housing_schedule: { address: @housing_schedule.address, census: @housing_schedule.census, email: @housing_schedule.email, enumeration_block_number: @housing_schedule.enumeration_block_number, enumeration_sub_block_number: @housing_schedule.enumeration_sub_block_number, enumerator: @housing_schedule.enumerator, filing_date: @housing_schedule.filing_date, form_number: @housing_schedule.form_number, houselist_block_number: @housing_schedule.houselist_block_number, phone: @housing_schedule.phone, respondent: @housing_schedule.respondent, serial_number: @housing_schedule.serial_number, status: @housing_schedule.status, supervisor: @housing_schedule.supervisor } }
    end

    assert_redirected_to housing_schedule_url(HousingSchedule.last)
  end

  test "should show housing_schedule" do
    get housing_schedule_url(@housing_schedule)
    assert_response :success
  end

  test "should get edit" do
    get edit_housing_schedule_url(@housing_schedule)
    assert_response :success
  end

  test "should update housing_schedule" do
    patch housing_schedule_url(@housing_schedule), params: { housing_schedule: { address: @housing_schedule.address, census: @housing_schedule.census, email: @housing_schedule.email, enumeration_block_number: @housing_schedule.enumeration_block_number, enumeration_sub_block_number: @housing_schedule.enumeration_sub_block_number, enumerator: @housing_schedule.enumerator, filing_date: @housing_schedule.filing_date, form_number: @housing_schedule.form_number, houselist_block_number: @housing_schedule.houselist_block_number, phone: @housing_schedule.phone, respondent: @housing_schedule.respondent, serial_number: @housing_schedule.serial_number, status: @housing_schedule.status, supervisor: @housing_schedule.supervisor } }
    assert_redirected_to housing_schedule_url(@housing_schedule)
  end

  test "should destroy housing_schedule" do
    assert_difference('HousingSchedule.count', -1) do
      delete housing_schedule_url(@housing_schedule)
    end

    assert_redirected_to housing_schedules_url
  end
end
