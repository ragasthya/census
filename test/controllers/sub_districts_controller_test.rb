require 'test_helper'

class SubDistrictsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sub_district = sub_districts(:one)
  end

  test "should get index" do
    get sub_districts_url
    assert_response :success
  end

  test "should get new" do
    get new_sub_district_url
    assert_response :success
  end

  test "should create sub_district" do
    assert_difference('SubDistrict.count') do
      post sub_districts_url, params: { sub_district: { area: @sub_district.area, density: @sub_district.density, district: @sub_district.district, name: @sub_district.name, population: @sub_district.population } }
    end

    assert_redirected_to sub_district_url(SubDistrict.last)
  end

  test "should show sub_district" do
    get sub_district_url(@sub_district)
    assert_response :success
  end

  test "should get edit" do
    get edit_sub_district_url(@sub_district)
    assert_response :success
  end

  test "should update sub_district" do
    patch sub_district_url(@sub_district), params: { sub_district: { area: @sub_district.area, density: @sub_district.density, district: @sub_district.district, name: @sub_district.name, population: @sub_district.population } }
    assert_redirected_to sub_district_url(@sub_district)
  end

  test "should destroy sub_district" do
    assert_difference('SubDistrict.count', -1) do
      delete sub_district_url(@sub_district)
    end

    assert_redirected_to sub_districts_url
  end
end
