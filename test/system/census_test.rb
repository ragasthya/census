require "application_system_test_case"

class CensusTest < ApplicationSystemTestCase
  setup do
    @censu = census(:one)
  end

  test "visiting the index" do
    visit census_url
    assert_selector "h1", text: "Census"
  end

  test "creating a Censu" do
    visit census_url
    click_on "New Censu"

    fill_in "Date,", with: @censu.date,
    fill_in "Date end", with: @censu.date_end
    fill_in "Date start", with: @censu.date_start
    fill_in "String", with: @censu.string
    fill_in "Year", with: @censu.year
    click_on "Create Censu"

    assert_text "Censu was successfully created"
    click_on "Back"
  end

  test "updating a Censu" do
    visit census_url
    click_on "Edit", match: :first

    fill_in "Date,", with: @censu.date,
    fill_in "Date end", with: @censu.date_end
    fill_in "Date start", with: @censu.date_start
    fill_in "String", with: @censu.string
    fill_in "Year", with: @censu.year
    click_on "Update Censu"

    assert_text "Censu was successfully updated"
    click_on "Back"
  end

  test "destroying a Censu" do
    visit census_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Censu was successfully destroyed"
  end
end
