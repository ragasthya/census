require "application_system_test_case"

class EntryHousesTest < ApplicationSystemTestCase
  setup do
    @entry_house = entry_houses(:one)
  end

  test "visiting the index" do
    visit entry_houses_url
    assert_selector "h1", text: "Entry Houses"
  end

  test "creating a Entry house" do
    visit entry_houses_url
    click_on "New Entry House"

    fill_in "Actual house use", with: @entry_house.actual_house_use
    fill_in "Address", with: @entry_house.address
    fill_in "Backward", with: @entry_house.backward
    fill_in "Banking", with: @entry_house.banking
    fill_in "Bathing", with: @entry_house.bathing
    fill_in "Bicycle", with: @entry_house.bicycle
    fill_in "Car", with: @entry_house.car
    fill_in "Census house number", with: @entry_house.census_house_number
    fill_in "Computer", with: @entry_house.computer
    fill_in "Condition", with: @entry_house.condition
    fill_in "Cooking fuel", with: @entry_house.cooking_fuel
    fill_in "Drinking water", with: @entry_house.drinking_water
    fill_in "Dwelling rooms", with: @entry_house.dwelling_rooms
    fill_in "Gender", with: @entry_house.gender
    fill_in "House head", with: @entry_house.house_head
    fill_in "House number", with: @entry_house.house_number
    fill_in "House use", with: @entry_house.house_use
    fill_in "Housing schedule", with: @entry_house.housing_schedule
    fill_in "Kitchen", with: @entry_house.kitchen
    fill_in "Lighting", with: @entry_house.lighting
    fill_in "Married couples", with: @entry_house.married_couples
    fill_in "Moped", with: @entry_house.moped
    fill_in "Ownership", with: @entry_house.ownership
    fill_in "Predominant material floor", with: @entry_house.predominant_material_floor
    fill_in "Predominant material roof", with: @entry_house.predominant_material_roof
    fill_in "Predominant material wall", with: @entry_house.predominant_material_wall
    fill_in "Radio", with: @entry_house.radio
    fill_in "Residing f", with: @entry_house.residing_f
    fill_in "Residing m", with: @entry_house.residing_m
    fill_in "Residing o", with: @entry_house.residing_o
    fill_in "Sewage connection", with: @entry_house.sewage_connection
    fill_in "Telephone", with: @entry_house.telephone
    fill_in "Television", with: @entry_house.television
    fill_in "Toilet", with: @entry_house.toilet
    click_on "Create Entry house"

    assert_text "Entry house was successfully created"
    click_on "Back"
  end

  test "updating a Entry house" do
    visit entry_houses_url
    click_on "Edit", match: :first

    fill_in "Actual house use", with: @entry_house.actual_house_use
    fill_in "Address", with: @entry_house.address
    fill_in "Backward", with: @entry_house.backward
    fill_in "Banking", with: @entry_house.banking
    fill_in "Bathing", with: @entry_house.bathing
    fill_in "Bicycle", with: @entry_house.bicycle
    fill_in "Car", with: @entry_house.car
    fill_in "Census house number", with: @entry_house.census_house_number
    fill_in "Computer", with: @entry_house.computer
    fill_in "Condition", with: @entry_house.condition
    fill_in "Cooking fuel", with: @entry_house.cooking_fuel
    fill_in "Drinking water", with: @entry_house.drinking_water
    fill_in "Dwelling rooms", with: @entry_house.dwelling_rooms
    fill_in "Gender", with: @entry_house.gender
    fill_in "House head", with: @entry_house.house_head
    fill_in "House number", with: @entry_house.house_number
    fill_in "House use", with: @entry_house.house_use
    fill_in "Housing schedule", with: @entry_house.housing_schedule
    fill_in "Kitchen", with: @entry_house.kitchen
    fill_in "Lighting", with: @entry_house.lighting
    fill_in "Married couples", with: @entry_house.married_couples
    fill_in "Moped", with: @entry_house.moped
    fill_in "Ownership", with: @entry_house.ownership
    fill_in "Predominant material floor", with: @entry_house.predominant_material_floor
    fill_in "Predominant material roof", with: @entry_house.predominant_material_roof
    fill_in "Predominant material wall", with: @entry_house.predominant_material_wall
    fill_in "Radio", with: @entry_house.radio
    fill_in "Residing f", with: @entry_house.residing_f
    fill_in "Residing m", with: @entry_house.residing_m
    fill_in "Residing o", with: @entry_house.residing_o
    fill_in "Sewage connection", with: @entry_house.sewage_connection
    fill_in "Telephone", with: @entry_house.telephone
    fill_in "Television", with: @entry_house.television
    fill_in "Toilet", with: @entry_house.toilet
    click_on "Update Entry house"

    assert_text "Entry house was successfully updated"
    click_on "Back"
  end

  test "destroying a Entry house" do
    visit entry_houses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Entry house was successfully destroyed"
  end
end
