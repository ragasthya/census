require "application_system_test_case"

class EntryPeopleTest < ApplicationSystemTestCase
  setup do
    @entry_person = entry_people(:one)
  end

  test "visiting the index" do
    visit entry_people_url
    assert_selector "h1", text: "Entry People"
  end

  test "creating a Entry person" do
    visit entry_people_url
    click_on "New Entry Person"

    fill_in "Age", with: @entry_person.age
    fill_in "Age at marriage", with: @entry_person.age_at_marriage
    fill_in "Attendence", with: @entry_person.attendence
    fill_in "Backward", with: @entry_person.backward
    fill_in "Birth place", with: @entry_person.birth_place
    fill_in "Born d", with: @entry_person.born_d
    fill_in "Born s", with: @entry_person.born_s
    fill_in "Company type", with: @entry_person.company_type
    fill_in "Date of birth", with: @entry_person.date_of_birth
    fill_in "Disability", with: @entry_person.disability
    fill_in "Employee class", with: @entry_person.employee_class
    fill_in "Employment seeking", with: @entry_person.employment_seeking
    fill_in "Employment status", with: @entry_person.employment_status
    fill_in "Employment type", with: @entry_person.employment_type
    fill_in "Gender", with: @entry_person.gender
    fill_in "Highest education", with: @entry_person.highest_education
    fill_in "Household schedule", with: @entry_person.household_schedule
    fill_in "Is head", with: @entry_person.is_head
    fill_in "Is literate", with: @entry_person.is_literate
    fill_in "Last residence", with: @entry_person.last_residence
    fill_in "Marital status", with: @entry_person.marital_status
    fill_in "Migration reason", with: @entry_person.migration_reason
    fill_in "Mode of travel", with: @entry_person.mode_of_travel
    fill_in "Name", with: @entry_person.name
    fill_in "Non economic activity", with: @entry_person.non_economic_activity
    fill_in "Occupation", with: @entry_person.occupation
    fill_in "Recent birth d", with: @entry_person.recent_birth_d
    fill_in "Recent birth s", with: @entry_person.recent_birth_s
    fill_in "Religion", with: @entry_person.religion
    fill_in "Residing since", with: @entry_person.residing_since
    fill_in "Surviving d", with: @entry_person.surviving_d
    fill_in "Surviving s", with: @entry_person.surviving_s
    click_on "Create Entry person"

    assert_text "Entry person was successfully created"
    click_on "Back"
  end

  test "updating a Entry person" do
    visit entry_people_url
    click_on "Edit", match: :first

    fill_in "Age", with: @entry_person.age
    fill_in "Age at marriage", with: @entry_person.age_at_marriage
    fill_in "Attendence", with: @entry_person.attendence
    fill_in "Backward", with: @entry_person.backward
    fill_in "Birth place", with: @entry_person.birth_place
    fill_in "Born d", with: @entry_person.born_d
    fill_in "Born s", with: @entry_person.born_s
    fill_in "Company type", with: @entry_person.company_type
    fill_in "Date of birth", with: @entry_person.date_of_birth
    fill_in "Disability", with: @entry_person.disability
    fill_in "Employee class", with: @entry_person.employee_class
    fill_in "Employment seeking", with: @entry_person.employment_seeking
    fill_in "Employment status", with: @entry_person.employment_status
    fill_in "Employment type", with: @entry_person.employment_type
    fill_in "Gender", with: @entry_person.gender
    fill_in "Highest education", with: @entry_person.highest_education
    fill_in "Household schedule", with: @entry_person.household_schedule
    fill_in "Is head", with: @entry_person.is_head
    fill_in "Is literate", with: @entry_person.is_literate
    fill_in "Last residence", with: @entry_person.last_residence
    fill_in "Marital status", with: @entry_person.marital_status
    fill_in "Migration reason", with: @entry_person.migration_reason
    fill_in "Mode of travel", with: @entry_person.mode_of_travel
    fill_in "Name", with: @entry_person.name
    fill_in "Non economic activity", with: @entry_person.non_economic_activity
    fill_in "Occupation", with: @entry_person.occupation
    fill_in "Recent birth d", with: @entry_person.recent_birth_d
    fill_in "Recent birth s", with: @entry_person.recent_birth_s
    fill_in "Religion", with: @entry_person.religion
    fill_in "Residing since", with: @entry_person.residing_since
    fill_in "Surviving d", with: @entry_person.surviving_d
    fill_in "Surviving s", with: @entry_person.surviving_s
    click_on "Update Entry person"

    assert_text "Entry person was successfully updated"
    click_on "Back"
  end

  test "destroying a Entry person" do
    visit entry_people_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Entry person was successfully destroyed"
  end
end
