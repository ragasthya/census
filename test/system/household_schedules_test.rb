require "application_system_test_case"

class HouseholdSchedulesTest < ApplicationSystemTestCase
  setup do
    @household_schedule = household_schedules(:one)
  end

  test "visiting the index" do
    visit household_schedules_url
    assert_selector "h1", text: "Household Schedules"
  end

  test "creating a Household schedule" do
    visit household_schedules_url
    click_on "New Household Schedule"

    fill_in "Address", with: @household_schedule.address
    fill_in "Census", with: @household_schedule.census
    fill_in "Details", with: @household_schedule.details
    fill_in "Email", with: @household_schedule.email
    fill_in "Enumeration block number", with: @household_schedule.enumeration_block_number
    fill_in "Enumeration sub block number", with: @household_schedule.enumeration_sub_block_number
    fill_in "Enumerator", with: @household_schedule.enumerator
    fill_in "Filing date", with: @household_schedule.filing_date
    fill_in "Form number", with: @household_schedule.form_number
    fill_in "Houselist block number", with: @household_schedule.houselist_block_number
    fill_in "Illiterate f", with: @household_schedule.illiterate_f
    fill_in "Illiterate m", with: @household_schedule.illiterate_m
    fill_in "Illiterate o", with: @household_schedule.illiterate_o
    fill_in "Literate f", with: @household_schedule.literate_f
    fill_in "Literate m", with: @household_schedule.literate_m
    fill_in "Literate o", with: @household_schedule.literate_o
    fill_in "Phone", with: @household_schedule.phone
    fill_in "Population f", with: @household_schedule.population_f
    fill_in "Population m", with: @household_schedule.population_m
    fill_in "Population o", with: @household_schedule.population_o
    fill_in "Respondent", with: @household_schedule.respondent
    fill_in "Serial number", with: @household_schedule.serial_number
    fill_in "Status", with: @household_schedule.status
    fill_in "Supervisor", with: @household_schedule.supervisor
    fill_in "Type", with: @household_schedule.type
    fill_in "Young f", with: @household_schedule.young_f
    fill_in "Young m", with: @household_schedule.young_m
    fill_in "Young o", with: @household_schedule.young_o
    click_on "Create Household schedule"

    assert_text "Household schedule was successfully created"
    click_on "Back"
  end

  test "updating a Household schedule" do
    visit household_schedules_url
    click_on "Edit", match: :first

    fill_in "Address", with: @household_schedule.address
    fill_in "Census", with: @household_schedule.census
    fill_in "Details", with: @household_schedule.details
    fill_in "Email", with: @household_schedule.email
    fill_in "Enumeration block number", with: @household_schedule.enumeration_block_number
    fill_in "Enumeration sub block number", with: @household_schedule.enumeration_sub_block_number
    fill_in "Enumerator", with: @household_schedule.enumerator
    fill_in "Filing date", with: @household_schedule.filing_date
    fill_in "Form number", with: @household_schedule.form_number
    fill_in "Houselist block number", with: @household_schedule.houselist_block_number
    fill_in "Illiterate f", with: @household_schedule.illiterate_f
    fill_in "Illiterate m", with: @household_schedule.illiterate_m
    fill_in "Illiterate o", with: @household_schedule.illiterate_o
    fill_in "Literate f", with: @household_schedule.literate_f
    fill_in "Literate m", with: @household_schedule.literate_m
    fill_in "Literate o", with: @household_schedule.literate_o
    fill_in "Phone", with: @household_schedule.phone
    fill_in "Population f", with: @household_schedule.population_f
    fill_in "Population m", with: @household_schedule.population_m
    fill_in "Population o", with: @household_schedule.population_o
    fill_in "Respondent", with: @household_schedule.respondent
    fill_in "Serial number", with: @household_schedule.serial_number
    fill_in "Status", with: @household_schedule.status
    fill_in "Supervisor", with: @household_schedule.supervisor
    fill_in "Type", with: @household_schedule.type
    fill_in "Young f", with: @household_schedule.young_f
    fill_in "Young m", with: @household_schedule.young_m
    fill_in "Young o", with: @household_schedule.young_o
    click_on "Update Household schedule"

    assert_text "Household schedule was successfully updated"
    click_on "Back"
  end

  test "destroying a Household schedule" do
    visit household_schedules_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Household schedule was successfully destroyed"
  end
end
