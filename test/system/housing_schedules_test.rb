require "application_system_test_case"

class HousingSchedulesTest < ApplicationSystemTestCase
  setup do
    @housing_schedule = housing_schedules(:one)
  end

  test "visiting the index" do
    visit housing_schedules_url
    assert_selector "h1", text: "Housing Schedules"
  end

  test "creating a Housing schedule" do
    visit housing_schedules_url
    click_on "New Housing Schedule"

    fill_in "Address", with: @housing_schedule.address
    fill_in "Census", with: @housing_schedule.census
    fill_in "Email", with: @housing_schedule.email
    fill_in "Enumeration block number", with: @housing_schedule.enumeration_block_number
    fill_in "Enumeration sub block number", with: @housing_schedule.enumeration_sub_block_number
    fill_in "Enumerator", with: @housing_schedule.enumerator
    fill_in "Filing date", with: @housing_schedule.filing_date
    fill_in "Form number", with: @housing_schedule.form_number
    fill_in "Houselist block number", with: @housing_schedule.houselist_block_number
    fill_in "Phone", with: @housing_schedule.phone
    fill_in "Respondent", with: @housing_schedule.respondent
    fill_in "Serial number", with: @housing_schedule.serial_number
    fill_in "Status", with: @housing_schedule.status
    fill_in "Supervisor", with: @housing_schedule.supervisor
    click_on "Create Housing schedule"

    assert_text "Housing schedule was successfully created"
    click_on "Back"
  end

  test "updating a Housing schedule" do
    visit housing_schedules_url
    click_on "Edit", match: :first

    fill_in "Address", with: @housing_schedule.address
    fill_in "Census", with: @housing_schedule.census
    fill_in "Email", with: @housing_schedule.email
    fill_in "Enumeration block number", with: @housing_schedule.enumeration_block_number
    fill_in "Enumeration sub block number", with: @housing_schedule.enumeration_sub_block_number
    fill_in "Enumerator", with: @housing_schedule.enumerator
    fill_in "Filing date", with: @housing_schedule.filing_date
    fill_in "Form number", with: @housing_schedule.form_number
    fill_in "Houselist block number", with: @housing_schedule.houselist_block_number
    fill_in "Phone", with: @housing_schedule.phone
    fill_in "Respondent", with: @housing_schedule.respondent
    fill_in "Serial number", with: @housing_schedule.serial_number
    fill_in "Status", with: @housing_schedule.status
    fill_in "Supervisor", with: @housing_schedule.supervisor
    click_on "Update Housing schedule"

    assert_text "Housing schedule was successfully updated"
    click_on "Back"
  end

  test "destroying a Housing schedule" do
    visit housing_schedules_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Housing schedule was successfully destroyed"
  end
end
