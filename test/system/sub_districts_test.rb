require "application_system_test_case"

class SubDistrictsTest < ApplicationSystemTestCase
  setup do
    @sub_district = sub_districts(:one)
  end

  test "visiting the index" do
    visit sub_districts_url
    assert_selector "h1", text: "Sub Districts"
  end

  test "creating a Sub district" do
    visit sub_districts_url
    click_on "New Sub District"

    fill_in "Area", with: @sub_district.area
    fill_in "Density", with: @sub_district.density
    fill_in "District", with: @sub_district.district
    fill_in "Name", with: @sub_district.name
    fill_in "Population", with: @sub_district.population
    click_on "Create Sub district"

    assert_text "Sub district was successfully created"
    click_on "Back"
  end

  test "updating a Sub district" do
    visit sub_districts_url
    click_on "Edit", match: :first

    fill_in "Area", with: @sub_district.area
    fill_in "Density", with: @sub_district.density
    fill_in "District", with: @sub_district.district
    fill_in "Name", with: @sub_district.name
    fill_in "Population", with: @sub_district.population
    click_on "Update Sub district"

    assert_text "Sub district was successfully updated"
    click_on "Back"
  end

  test "destroying a Sub district" do
    visit sub_districts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sub district was successfully destroyed"
  end
end
